import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new HomePage(),
      debugShowCheckedModeBanner: false,
      title: 'Calculateur de calories',
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  bool isMan = true;
  int age;

  double taille = 100;

  int selected = 0;

  String value = '';

  int tailleCm = 100;

  DateTime date;

  double poids;

  int caloriesSport = 77777;
  int caloriesBase = 5000;


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Center(child: new Text('Calculateur de calories')),
          backgroundColor: setColor(),
        ),
//      backgroundColor: isWoman ? Colors.pink : Colors.blue,
        body: new SingleChildScrollView(
          padding: EdgeInsets.all(5.0),
          child: new Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                padding(),
                new Text(
                    'Remplissez tous les champs pour obtenir votre besoin journalier en calories.',
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                      fontWeight: FontWeight.bold,
                    )
                ),
                padding(),
                new Card(
                    elevation: 10.0,
                    child: new Center(
                      child: new Column(children: <Widget>[
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            new Text('Femme',
                                style: TextStyle(color: Colors.pink)),
                            new Switch(
                              value: isMan,
                              onChanged: (bool newValue) {
                                setState(() {
                                  isMan = newValue;
                                });
                              },
                              activeColor: Colors.blue,
                              inactiveTrackColor: Colors.pink,
                            ),
                            new Text('Homme',
                                style: TextStyle(color: Colors.blue))
                          ],
                        ),
                        padding(),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new RaisedButton(
                              color: setColor(),
                              onPressed: () {
                                showDateDialog(context);
                              },
                              child: buildText(),
                            )
                          ],
                        ),
                        padding(),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            textWithStyle('Votre taille est de: $tailleCm cm'),
                          ],
                        ),
                        padding(),
                        new Slider(
                            activeColor: setColor(),
                            value: taille,
                            min: 100,
                            max: 215,
                            onChanged: (double newValue) {
                              setState(() {
                                taille = newValue;
                                tailleCm = taille.toInt();
                              });
                            }
                        ),
                        padding(),
                        new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Text('Entrez votre poids en kilos.')
                            ]),
                        new Row(
                          children: <Widget>[
                            new Expanded(
                                child: new TextField(
                              cursorColor: setColor(),
                              keyboardType: TextInputType.number,
                              onChanged: (String newText) {
                                setState(() {
                                  poids = double.tryParse(newText);
                                });
                              },
                            ))
                          ],
                        ),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            textWithStyle('Quelle est votre activité sportive?')
                          ],
                        ),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            radioWithStyle(1),
                            radioWithStyle(2),
                            radioWithStyle(3),
                          ],
                        ),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            textWithStyle('Faible', color: setColor()),
                            textWithStyle('Modérée', color: setColor()),
                            textWithStyle('Forte', color: setColor()),
                          ],
                        )
                      ]),
                    )),
                padding(),
                new Center(
                  child: new RaisedButton(
                    color: setColor(),
                    onPressed: () {
                      if (age != null && poids != null && taille != null && selected != null) {
                        calculate();
                      } else {
                        alert();
                      }
                    },
                    child: new Text('Calculer',
                        style: TextStyle(color: Colors.white)),
                  ),
                )
              ],
            ),
          ),
        ));
  }

  Text buildText() {
    return new Text(
        age == null ? 'Appuyez pour entrer votre âge.' : 'Votre age est : $age',
        style: TextStyle(color: Colors.white));
  }

  Text textWithStyle(String text, {color : Colors.black}) {
    return new Text(text, style: TextStyle(color: color), textAlign: TextAlign.center);
  }

  Radio radioWithStyle(int value) {
    return new Radio(
        value: value,
        activeColor: setColor(),
        groupValue: selected,
        onChanged: (newValue) {
          setState(() {
            selected = newValue;
          });
        });
  }

  Future<Null> alert() async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (value) {
        return new AlertDialog(
          title: textWithStyle('Erreur'),
          content: Text('Tous les champs ne sont pas remplis'),
          actions: <Widget>[
            new FlatButton(
                onPressed: () => Navigator.pop(context),
                child: Text('OK', style: TextStyle(color: Colors.red)))
          ],
        );
      }
    );
  }

  void calculate() async {
    calculateCalories();
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext bc) {
          return SimpleDialog(
            title: textWithStyle('Votre besoin en calories', color: setColor()),
            children: <Widget>[
              padding(),
              textWithStyle('Votre besoin de base est de : $caloriesBase.'),
              padding(),
              textWithStyle('Votre besoin avec activité sportive : $caloriesSport.'),
              padding(),
              RaisedButton(
                onPressed: () => Navigator.pop(context),
                child: Text('OK'),
                color: setColor(),
              )
            ],
          );
        }
    );
  }

  Widget createDialog(Text title, String content) {
    return new AlertDialog(
      title: title,
      content: Text(content),
      actions: <Widget>[
        new FlatButton(
            onPressed: () => Navigator.pop(context),
            child: Text('OK', style: TextStyle(color: Colors.red)))
      ],
    );
  }

  Future<Null> showDateDialog(BuildContext context) async {
    DateTime d = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1920),
      lastDate: DateTime.now(),
      initialDatePickerMode: DatePickerMode.year
    );
    setState(() {
      if (d != null)
        age = DateTime.now().year - d.year;
    });
  }

  Color setColor() {
    return isMan ? Colors.blue : Colors.pink;
  }

  Padding padding() {
    return new Padding(padding: EdgeInsets.only(top: 20.0));
  }

  void calculateCalories() {
    caloriesBase = (66.4730 + (13.7516 * poids) + (5.0033 * taille) - (6.7550 * age)).toInt();
    caloriesSport = (655.0955 + (9.5634 * poids) + (1.8495 * taille) - (4.6756 * age)).toInt();
    switch (selected) {
      case 1: caloriesSport = (caloriesBase * 1.2).toInt(); break;
      case 2: caloriesSport = (caloriesBase * 1.5).toInt(); break;
      case 3: caloriesSport = (caloriesBase * 1.8).toInt(); break;
      default: caloriesSport = caloriesBase; break;
    }
    setState(() {

    });
  }

}
